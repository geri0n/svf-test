#include <llvm/Support/CommandLine.h>
#include <vector>
#include <string>
#include <SVF-FE/PAGBuilder.h>
#include <WPA/Andersen.h>
#include <llvm/IR/ValueSymbolTable.h>

static llvm::cl::opt<std::string> InputFilename(llvm::cl::Positional,
        llvm::cl::desc("<input bitcode>"), llvm::cl::init("-"));

const SVF::VFGNode* get_vfg_node(const SVF::SVFG& vfg, const llvm::Value* value) {
	SVF::PAG* pag = SVF::PAG::getPAG();
	SVF::PAGNode* pNode = pag->getPAGNode(pag->getValueNode(value));
	assert(pNode != nullptr);
	const SVF::VFGNode* vNode = vfg.getDefSVFGNode(pNode);
	assert(vNode != nullptr);
	return vNode;
}

int main(int argc, char ** argv) {
	int arg_num = 0;
	char **arg_value = new char*[argc];
	std::vector<std::string> moduleNameVec;
	SVF::SVFUtil::processArguments(argc, argv, arg_num, arg_value, moduleNameVec);
	llvm::cl::ParseCommandLineOptions(arg_num, arg_value,
                                "Whole Program Points-to Analysis\n");

	SVF::SVFModule* svfModule = SVF::LLVMModuleSet::getLLVMModuleSet()->buildSVFModule(moduleNameVec);
	llvm::Module* llvmModule = SVF::LLVMModuleSet::getLLVMModuleSet()->getMainLLVMModule();

	SVF::PAGBuilder builder;
	SVF::PAG *pag = builder.build(svfModule);

	SVF::Andersen *ander = SVF::AndersenWaveDiff::createAndersenWaveDiff(pag);

	/// Sparse value-flow graph (SVFG)
	SVF::SVFGBuilder svfBuilder;
	SVF::SVFG& svfg = *svfBuilder.buildFullSVFG(ander);
	svfg.dump("svfg");

	for (const auto& [_, node] : svfg) {
		llvm::errs() << "Node " << *node << "\n";
		if (auto s_node = llvm::dyn_cast<SVF::StmtVFGNode>(node)) {
			const llvm::Value* val = s_node->getPAGEdge()->getValue();
			// TODO does not work
			// auto n_node = get_vfg_node(svfg, val);
			// assert(s_node == n_node);
		} else if (auto s_node = llvm::dyn_cast<SVF::ActualParmVFGNode>(node)) {
			const llvm::Value* val = s_node->getParam()->getValue();
			auto n_node = get_vfg_node(svfg, val);
			assert(s_node == n_node);
		} else if (auto s_node = llvm::dyn_cast<SVF::FormalParmVFGNode>(node)) {
			const llvm::Value* val = s_node->getParam()->getValue();
			auto n_node = get_vfg_node(svfg, val);
			assert(s_node == n_node);
		} else if (auto s_node = llvm::dyn_cast<SVF::ActualRetVFGNode>(node)) {
			// TODO getRet vs getRev in ActualRet vs FormatRet?
			const llvm::Value* val = s_node->getRev()->getValue();
			auto n_node = get_vfg_node(svfg, val);
			assert(s_node == n_node);
		} else if (auto s_node = llvm::dyn_cast<SVF::FormalRetVFGNode>(node)) {
			const llvm::Value* val = s_node->getRet()->getValue();
			auto n_node = get_vfg_node(svfg, val);
			assert(s_node == n_node);
		} else if (auto s_node = llvm::dyn_cast<SVF::BinaryOPVFGNode>(node)) {
			const llvm::Value* val = s_node->getRes()->getValue();
			auto n_node = get_vfg_node(svfg, val);
			assert(s_node == n_node);
		} else if (auto s_node = llvm::dyn_cast<SVF::CmpVFGNode>(node)) {
			const llvm::Value* val = s_node->getRes()->getValue();
			auto n_node = get_vfg_node(svfg, val);
			assert(s_node == n_node);
		} else if (auto s_node = llvm::dyn_cast<SVF::UnaryOPVFGNode>(node)) {
			const llvm::Value* val = s_node->getRes()->getValue();
			auto n_node = get_vfg_node(svfg, val);
			assert(s_node == n_node);
		} else if (auto s_node = llvm::dyn_cast<SVF::PHIVFGNode>(node)) {
			const llvm::Value* val = s_node->getRes()->getValue();
			auto n_node = get_vfg_node(svfg, val);
			// TODO does not work
			// assert(s_node == n_node);
		} else if (auto s_node = llvm::dyn_cast<SVF::NullPtrVFGNode>(node)) {
			// no connected value (has PAGNode but no value)
		} else if (auto s_node = llvm::dyn_cast<SVF::MRSVFGNode>(node)) {
			// no connected value
		} else {
			assert(false && "Should not happen.");
		}
	}

	/*
	// TODO throws assertion
	// Is there a possibility to get as much as possible SVF Nodes?
	for (const auto& function : *llvmModule) {
		for (const auto& basic_block : function) {
			for (const auto& instruction : basic_block) {
				auto svf_node = get_vfg_node(svfg, &instruction);
			}
		}
	}
	*/

	for (const auto& value : llvmModule->getValueSymbolTable()) {
		auto svf_node = get_vfg_node(svfg, value.getValue());
	}
}
