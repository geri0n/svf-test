SVF Test
========

Created for this issue:
https://github.com/SVF-tools/SVF/issues/485

Usage:
```
./init
meson build && cd build
meson test
```
